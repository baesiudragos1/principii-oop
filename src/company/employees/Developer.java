package company.employees;

public abstract class Developer {

  private final String name;
  private String experience;
  private Integer salary;
  private String jobDescripsion;
  static final String COMPANY_NAME=" ";


    public String getName() {
        return name;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public String getJobDescripsion() {
        return jobDescripsion;
    }

    public void setJobDescripsion(String jobDescripsion) {
        this.jobDescripsion = jobDescripsion;
    }

    public Developer(String name, String experience, Integer salary, String jobDescripsion) {
        this.name = name;
        this.experience = experience;
        this.salary = salary;
        this.jobDescripsion = jobDescripsion;
    }


    /* public Developer() {

        System.out.println(" Constructor - Java Dragos Dragos Dragos Dragos ");


    }*/

    @Override
    public String toString() {
        return "Developer" +
                "name= " + name + '\'' +
                ", experience= " + experience + '\'' +
                ", salary= " + salary +
                ", jobDescripsion= " + jobDescripsion + '\'';

    }
}
